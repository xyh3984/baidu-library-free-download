import requests
from PIL import Image
import copy

log_file = "info.log"
img_list = []
f = open(log_file, "r")
links = []

for line in f:
    if "https" in line:
        link = line[line.find("https"): len(line)-1]
        links.append(link)

for i in range(len(links)):
    filename = str(i+1) + ".jpg"
    with open(filename, 'wb') as handle:
        response = requests.get(links[i], stream=True)
        if not response.ok:
            print(response)

        for block in response.iter_content(1024):
            if not block:
                break
            handle.write(block)

    try:
        im = Image.open(filename)
        img_list.append(copy.copy(im))
    except FileNotFoundError:
        pass

img_list[0].save("total.pdf", "PDF", resolution=100.0, save_all=True, append_images=img_list)

