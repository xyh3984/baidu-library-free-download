# 半自动免费下载百度文库付费内容代码

### 用法
1. 先在Chrome中打开百度文库的你想要的下载的链接。
2. F12打开console，并将下列js语句输入执行阻住百度文库收费跳转。

```
window.onbeforeunload = function(){ return 'Leave page?'; }; 
```
3. 在百度文库页面中一直点击“继续浏览”把所有内容加载出来。
4. 之后把js文件中的所有代码复制出来在console中执行，并把console的log保存到info.log中。
5. 最后运行py文件即可全部下载并转换pdf。